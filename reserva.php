<?php
	session_start();
	require_once('./php/functions.php');
	if(empty($_SESSION['id_usuario'])) header('Location:'.URL_SITE);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reserva</title>
	<script type='text/javascript' src='./js/miajax.js'></script>
	<link rel="stylesheet" type="text/css" href="./css/estilo.css"/>
</head>
<body>
	<header id="move">
		<div id="contador">
		<div class="centrado">
			<h1 id="titulo">despegue en directo</h1>
			<h1 id="calor">una experiencia increible <span>compra ahora tu entrada</span></h1>
		</div>		
	</div>		
	</header>
		<section class="sesion">
		<div class="centrado">
			<p>Bienvenido/a de nuevo: <em><?php echo $_SESSION['name']; ?></em></p>
			<a class="salir" href="./php/cerrarSesion.php"> Cerrar Sesion </a>
		</div>		
	</section>
	<section id="contentAsientos">
		<div class="centrado">
			<h1>Selecciona tu asiento</h1>
			<div id="asientos"></div>
			<p>* Reserve y confirme su asiento antes de 90segundos (1,5 minutos)</p>
			<br>
			<br>
			<a class="confirmar" href="./confirmar.php"> Confirmar y Pagar </a>
		</div>
	</section>
	
	<footer>
		
	</footer>
</body>
<script type="text/javascript">
	//window.onload = jsCargarAsientos();	
	window.onload = refresca();
</script>
</html>