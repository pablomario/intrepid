<?php
	session_start();
	require_once('./php/functions.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Mi AjaX</title>
	<script type='text/javascript' src='./js/miajax.js'></script>
	<link rel="stylesheet" type="text/css" href="./css/estilo.css"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type='text/javascript' src='./jquery.particleground.js'></script>
  	<script type='text/javascript' src='js/demo.js'></script>
</head>
<body>
	<div id="contador">
		<div class="centrado">
			<h1 id="titulo">despegue en directo</h1>
			<h1 id="calor">una experiencia increible <span>compra ahora tu entrada</span></h1>
			<h1 id="cuentatras">Tiempo restante: <span id="tiempo"></span></h1>
		</div>		
	</div>

	<section class="jumbotron">
	<div  id="particles"></div>
		<div class="centrado">
			<aside>	
				<img src="./img/logotipo.png">			
				<?php
					if(!empty($_SESSION['id_usuario'])){
						//echo "EL usuario es: ".$_SESSION['nombre']."<br>";
						//echo "El numero de tarjeta es: ".$_SESSION['creditcard'];
						echo '<h2>Usted ya ha iniciado sesion <a href="'.URL_SITE.'/reserva.php">Reservas</a></h2>';
					}else{
						echo '<form action="./php/login.php"  method="post">';
							echo '<input type="text" required placeholder="Nombre de usuario"name="name">';
							echo '<input type="password" required placeholder="Contraseña" name="password">';
							echo '<input type="submit" value="Iniciar Sesion">';	
						echo '</form>';
					}
				?>
			</aside>
		</div>
	</section>
	<section class="blog">
		<div class="centrado">
			<h1>Blog Intrepido</h1>
			<article>
				<h1>Lanzado el observatorio DSCOVR de la NASA</h1>
				<img src="./img/cohete.jpg">
				<p>El 11 de febrero de 2015 a las 23:03 UTC la empresa SpaceX lanzó el 
				observatorio espacial DSCOVR (Deep Space Climate Observatory) de la NASA y 
				la NOAA mediante un cohete Falcon 9R (v1.1) desde la rampa SLC-40 de Cabo 
				Cañaveral. DSCOVR promete mandar imágenes espectaculares de la Tierra de 
				forma continua desde 1,5 millones de kilómetros de distancia. Puesto que 
				el DSCOVR estará situado en el punto de Lagrange L1 del sistema Tierra-Sol,
				 se trata del primer lanzamiento de SpaceX más allá de la órbita 
				 geoestacionaria. DSCOVR llegará a su destino dentro de 110 días.</p>				
			</article>
			<aside>
				<img src="./img/insignia.png">
			</aside>
		</div>		
	</section>

	<footer>
		<div class="centrado">


		</div>
	</footer>


<script language="JavaScript">
	var anioFinal = 2018
	var mesFinal = 12
	var diaFinal = 25
	mesFinal -= 1
	function faltan(){
		fechaFinal = new Date(anioFinal,mesFinal,diaFinal)
		fechaActual = new Date()
		diferencia = fechaFinal - fechaActual
		diferenciaSegundos = diferencia /1000
		diferenciaMinutos = diferenciaSegundos/60
		diferenciaHoras = diferenciaMinutos/60
		diferenciaDias = diferenciaHoras/24
		diferenciaHoras2 = parseInt(diferenciaHoras) - (parseInt(diferenciaDias) *24)
		diferenciaMinutos2 = parseInt(diferenciaMinutos) - (parseInt(diferenciaHoras) * 60)
		diferenciaSegundos2 = parseInt(diferenciaSegundos) - (parseInt(diferenciaMinutos) * 60)
		diferenciaDias = parseInt(diferenciaDias)
		if (diferenciaDias < 10 && diferenciaDias > -1){diferenciaDias = "0" + diferenciaDias}
		if(diferenciaHoras2 < 10 && diferenciaHoras2 > -1){diferenciaHoras2 = "0" + diferenciaHoras2}
		if(diferenciaMinutos2 < 10 && diferenciaMinutos2 > -1){diferenciaMinutos2 = "0" + diferenciaMinutos2}
		if(diferenciaSegundos2 < 10 && diferenciaSegundos2 > -1){diferenciaSegundos2 = "0" + diferenciaSegundos2}
		if(diferenciaDias <= 0 && diferenciaHoras2<= 0 && diferenciaMinutos2 <= 0 && diferenciaSegundos2 <= 0){
			diferenciaDias = 0
			diferenciaHoras2 = 0
			diferenciaMinutos2 = 0
			diferenciaSegundos2 = 0
			document.getElementById('tiempo').innerHTML = diferenciaHoras2 + ':' + diferenciaMinutos2 + ':' + diferenciaSegundos2
		}else{
			document.getElementById('tiempo').innerHTML = diferenciaHoras2 + ':' + diferenciaMinutos2 + ':' + diferenciaSegundos2
			setTimeout('faltan()',1000)
		}
	}
	faltan();
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>