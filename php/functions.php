<?php


	define("DB_HOST","127.0.0.1");
	define("DB_USER","root");
	define("DB_PASSWORD","root");
	define("DB_NAME","mma");
	define("URL_SITE","http://127.0.0.1/intrepid");


	function conexion(){
		$conexion = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
		if(mysqli_connect_errno()){
			die("Error: " .mysqli_connect_errno());
		}
		return $conexion;
	}

	function login($usr, $pass){
		$conexion = conexion();
		$sql = 'SELECT * FROM usuarios WHERE name="'.$usr.'" AND password = "'.$pass.'" LIMIT 1;';
		if($resultado = $conexion->query($sql)){
			if($resultado->num_rows == 1){
				if($row = $resultado->fetch_array()){
					session_start();
					$_SESSION['id_usuario'] = $row[0];
					$_SESSION['name'] = $row[1];
					$_SESSION['creditcard'] = $row[3];
					header('Location:'.URL_SITE.'/reserva.php');
				}else{
					header('Location:'.URL_SITE);
				}
			}else{
				echo "no hay usuario";
			}			
		}
	}


	function f30sec(){
		$conexion = conexion();
		$sql= 'SELECT * FROM asientos';
		if($resultado = $conexion->query($sql) ){
			while($row = $resultado->fetch_array()){				
				$sql30sec= 'UPDATE asientos SET estado=0 , tiempo = null, usuario=0 WHERE estado<>2 and now()-tiempo>90 and id = '.$row[0].';';
				$conexion->query($sql30sec);								
			}			
		}else{
			echo 'Error al ejecutar Query';
		}		

	}


	function phpRefresca($usuario){
		f30sec();
		$conexion = conexion();
		$sql= 'SELECT * FROM asientos';
		if($resultado = $conexion->query($sql) ){
			while($row = $resultado->fetch_array()){				
				if($row[1]==0){					
					echo '<img onclick="jsActualizarAsiento('.$row[0].');" src="./img/verde.png">';
				}else if($row[1]==1){	
					if($row[3]==$usuario){
						echo '<img onclick="jsActualizarAsiento('.$row[0].');" src="./img/azul.png">';						
					}else{
						echo '<img src="./img/rojo.png">';
					}																		
				}else{
					echo '<img src="./img/rojo.png">';
				}											
			}			
		}else{
			echo 'Error al ejecutar Query';
		}		
	}


	function phpActualizarAsiento($asiento,$usuario){
		phpRefresca($usuario);	
		$conexion = conexion();	
		$sql= 'SELECT * FROM asientos WHERE id = '.$asiento.';';	
		if($resultado = $conexion->query($sql) ){			
			if($row = $resultado->fetch_array()){
				if($row[1]==0){					
					$sql= 'UPDATE asientos SET estado=1 , tiempo = now(), usuario='.$usuario.' WHERE id = '.$asiento.';';
					$conexion->query($sql);	
					phpRefresca($usuario);			
				}else if($row[1]==1){
					$sql= 'UPDATE asientos SET estado=0 , tiempo = null, usuario = 0 WHERE id = '.$asiento.';';
					$conexion->query($sql);
					phpRefresca($usuario);
				}else{
					// Esta ocupado
				}	
				phpRefresca($usuario);			
			}
		}else{
			echo 'Error al ejecutar Query';
		}
	}


	function phpConfirmar($usuario){		
		$conexion = conexion();	
		$sql= 'SELECT * FROM asientos WHERE usuario = '.$usuario.';';	
		if($resultado = $conexion->query($sql) ){			
			while($row = $resultado->fetch_array()){								
				$sql= 'UPDATE asientos SET estado=2 , tiempo = now(), usuario='.$usuario.' WHERE id = '.$row[0].';';
				$conexion->query($sql);								
			}
			echo "<h2>Pago realizado correctamente</h2>";
		}else{
			echo '<h2>Error al procesar la operación, intentelo más tarde</h2>';
		}
		$conexion->close();
	}

	



?>