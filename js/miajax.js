
var objAjax;	// el objeto Ajax (XMLHttpRequest)
var url_site = "http://127.0.0.1/miajax";


function holajs(){
	alert("Estamos en JS");
}

	 
function CrearObjAjax(){
	try{
		objAjax=new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
	}catch (e){
		try{
			objAjax=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
		}catch (e){
			try{
				objAjax=new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e){
				alert("Tu explorador no soporta AJAX.");
				return false;
			}
		}
	}
}


function refresca(){
	CrearObjAjax();
	// Timestamp para que IE no impida la recarga
	var timestamp = parseInt(new Date().getTime().toString().substring(0, 10));
	var url = "./php/refresca.php?t="+timestamp;	
	objAjax.onreadystatechange=function(){
		// Si esta listo para recibir de nuevo la pagina, la refresca
		if(objAjax.readyState== 4 && objAjax.readyState != null){
			document.getElementById("asientos").innerHTML=objAjax.responseText;
			setTimeout('refresca()',1*3000);
		}
	}
	objAjax.open("GET",url,true);
	objAjax.send(null);
}




function jsActualizarAsiento(asiento){
	CrearObjAjax();	
	var url = "./php/actualizarAsiento.php?asiento="+asiento;		
	objAjax.onreadystatechange=function(){		
		if(objAjax.readyState== 4 && objAjax.readyState != null){
			document.getElementById("asientos").innerHTML=objAjax.responseText;	
		}
	}
	objAjax.open("GET",url,true);
	objAjax.send(null);
	refresca();
	
}


function jsConfirmar(){
	CrearObjAjax();
	var url = "./php/confirmar.php";
	objAjax.onreadystatechange=function(){		
		if(objAjax.readyState== 4 && objAjax.readyState != null){
			document.getElementById("asientos").innerHTML=objAjax.responseText;	
		}
	}
	objAjax.open("GET",url,true);
	objAjax.send(null);
}
