/**
 * Particleground demo
 * @author Jonathan Nicol - @mrjnicol
 */

$(document).ready(function() {
  $('#particles').particleground({
    dotColor: 'rgba(130,220,155,0.3)',
    lineColor: 'rgba(130,220,155,0.3)'
  });
  $('.intro').css({
    'margin-top': -($('.intro').height() / 2)
  });
});